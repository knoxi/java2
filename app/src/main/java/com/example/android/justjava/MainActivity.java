package com.example.android.justjava;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * This method is called when the order button is clicked.
     */
    int numOfCoffees = 0;
    int pricePerCoffee = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void submitOrder(View view) {
        String priceMessage = "Total: $"+numOfCoffees*pricePerCoffee+"\nThank You.";
        displayMessage(priceMessage);
        //display(numOfCoffees);
        //displayPrice(numOfCoffees * pricePerCoffee);


    }

    private void displayMessage(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(message);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int i) {
        TextView quantityTextView = (TextView) findViewById(
                R.id.quantity_text_view);
        quantityTextView.setText("" + i);
    }

    /*
    This method displays the given quantity value on the screen
     */
    private void displayPrice(int number) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }

    public void increment(View view) {
        numOfCoffees += 1;
        displayQuantity(numOfCoffees);
    }

    private void displayQuantity(int i) {
        TextView qty = (TextView) findViewById(R.id.quantity_text_view);
        qty.setText("" + i);
    }

    public void decrement(View view) {
        numOfCoffees += 1;
        displayQuantity(numOfCoffees);
    }
}
